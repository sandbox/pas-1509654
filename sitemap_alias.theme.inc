<?php
/**
 * @file
 * sitemap_alias.theme.inc
 *
 * Sitemap Alias theme functions.
 */

/**
 * 
 * Enter description here ...
 * @param unknown_type $variables
 */
function theme_sitemap_alias_tree($variables) {
  $items = $variables['items'];
  $title = $variables['title'];
  $type = $variables['type'];
  $attributes = $variables['attributes'];
  $output = '<div class="sitemap-item-list">';
  if (isset($title)) {
    $output .= '<h3>' . $title . '</h3>';
  }

  if (!empty($items)) {
    $output .= "<$type" . drupal_attributes($attributes) . '>';
    $num_items = count($items);
    $i = 0;
    foreach ($items as $item) {
      $attributes = array();
      $children = array();
      $data = '';
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            if (is_array($value)) {
              $data = l($value['title'], $value['href']['path'], $value['href']['options']);
            }
            else {
              $data = $value;
            }
          }
          elseif ($key == 'children') {
            $children = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $data = $item;
      }
      if (count($children) > 0) {
        // Render nested list.
        $data .= theme_sitemap_alias_tree(array('items' => $children, 'title' => NULL, 'type' => $type, 'attributes' => $attributes));
      }
      if ($i == 0) {
        $attributes['class'][] = 'first';
      }
      if ($i == $num_items - 1) {
        $attributes['class'][] = 'last';
      }
      $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
      $i++;
    }
    $output .= "</$type>";
  }
  $output .= '</div>';
  return $output;
}
