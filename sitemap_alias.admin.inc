<?php
function sitemap_alias_admin_settings() {
  // Fields to set the sitemap title and message.
  $form['sitemap_alias_page_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sitemap page settings'),
  );
  $form['sitemap_alias_page_settings']['sitemap_alias_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Page title'),
    '#default_value' => variable_get('sitemap_alias_page_title', t('Sitemap')),
    '#description' => t('Page title that will be used on the <a href="@link">sitemap page</a>.', array('@link' => url('sitemap'))),
  );
  $sitemap_message = variable_get('sitemap_alias_page_message', array('value' => '', 'format' => 'filtered_html'));
  $form['sitemap_alias_page_settings']['sitemap_alias_page_message'] = array(
    '#type' => 'text_format',
    '#format' => isset($sitemap_message['format']) ? $sitemap_message['format'] : 'filtered_html',
    '#title' => t('Sitemap header message'),
    '#default_value' => $sitemap_message['value'],
    '#description' => t('Define a message to be displayed above the sitemap.'),
  );
  $sitemap_footer_message = variable_get('sitemap_alias_footer_message', array('value' => '', 'format' => 'filtered_html'));
  $form['sitemap_alias_page_settings']['sitemap_alias_footer_message'] = array(
    '#type' => 'text_format',
    '#format' => isset($sitemap_footer_message['format']) ? $sitemap_footer_message['format'] : 'filtered_html',
    '#title' => t('Sitemap footer message'),
    '#default_value' => $sitemap_footer_message['value'],
    '#description' => t('Define a message to be displayed beneath the sitemap.'),
  );
  
  // Fields to select types to show in sitemap.
  $form['sitemap_alias_content'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sitemap content'),
  );

  // Node types
  $node_type_options = array();
  foreach (node_type_get_types() as $node) {
    $node_type_options[$node->type] = $node->name;
    $node_type_default_options[] = $node->type;
  }
  $form['sitemap_alias_content']['sitemap_alias_show_nodes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node types to include in the sitemap'),
    '#default_value' => variable_get('sitemap_alias_show_nodes', $node_type_default_options),
    '#options' => $node_type_options,
    '#multiple' => TRUE,
  );

  // Taxonomy terms.
  if (module_exists('taxonomy')) {
    $form['sitemap_alias_content']['taxonomy_heading'] = array(
      '#type' => 'markup',
      '#markup' => 'Categories to include in the sitemap',
      '#prefix' => '<label>',
      '#suffix' => '</label>',
    );
    
    $vocab_options = array();
    foreach (taxonomy_get_vocabularies() as $vocabulary) {
      $form['sitemap_alias_content']['sitemap_alias_show_voc_' . $vocabulary->machine_name] = array(
        '#type' => 'checkbox',
        '#title' => check_plain($vocabulary->name),
        '#default_value' => variable_get('sitemap_alias_show_voc_' . $vocabulary->machine_name, 0),
      );      
      
      $vocabulary_terms = taxonomy_get_tree($vocabulary->vid);
      if (!empty($vocabulary_terms)) {
        foreach ($vocabulary_terms as $term) {
          $term_options[$vocabulary->machine_name][$term->tid] =  $term->name;
          $term_default_options[$vocabulary->machine_name][] = $term->tid;
        }

        $form['sitemap_alias_content'][$vocabulary->machine_name . '_fieldset'] = array(
          '#type' => 'fieldset',
          '#title' => t('Select the terms from the \'@vocabulary_name\' vocabulary to include', array('@vocabulary_name' => $vocabulary->name)),
          '#states' => array(
            'visible' => array(
              ':input[name = "sitemap_alias_show_voc_' . $vocabulary->machine_name . '"]' => array('checked' => TRUE),
            ),
          ),
        );
  
        $form['sitemap_alias_content'][$vocabulary->machine_name . '_fieldset']['sitemap_alias_show_terms_' . $vocabulary->machine_name] = array(
          '#type' => 'checkboxes',
          '#title' => t('\'@vocabulary_name\' terms', array('@vocabulary_name' => $vocabulary->name)),
          '#default_value' => variable_get('sitemap_alias_show_terms_' . $vocabulary->machine_name, $term_default_options[$vocabulary->machine_name]),
          '#options' => $term_options[$vocabulary->machine_name],
          '#multiple' => TRUE,
        );
      }
    }  
  }

  // Users
  if (module_exists('user')) {
    $form['sitemap_alias_content']['sitemap_alias_show_users'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show users'),
      '#prefix' => '<label for="edit-sitemap-alias-show-duplicates">Show users</label>',
      '#default_value' => variable_get('sitemap_alias_show_users', 0),
    );
  }

  // Fields to set group, sort order and show duplicates.
  $form['sitemap_alias_other_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Other settings'),
  );
  
  $form['sitemap_alias_other_settings']['sitemap_alias_grouping'] = array(
    '#type' => 'radios',
    '#title' => t('Group options'),
    '#description' => t('Grouping per type will render a seperate tree per type with an optional heading above it, while selecting \'one sitemap tree\' will render them all in just one big tree with no heading.'),
    '#options' => array('full' => 'One sitemap tree', 'group' => 'Group per type'),
    '#default_value' => variable_get('sitemap_alias_grouping', 'group'),
  );

  $form['sitemap_alias_other_settings']['sitemap_alias_group_titles'] = array(
    '#type' => 'fieldset',
    '#title' => 'group per type titles',
    '#states' => array(
      'visible' => array(
        ':input[name = "sitemap_alias_grouping"]' => array('value' => 'group'),
      ),
    ),
    
  );
  $form['sitemap_alias_other_settings']['sitemap_alias_group_titles']['sitemap_alias_group_title_nodes'] = array(
    '#type' => 'textfield',
    '#title' => t('Node group title'),
    '#default_value' => variable_get('sitemap_alias_group_title_nodes', t('Content')),
    '#description' => t('The subtitle that will appear above the nodes when selecting a seperate tree per type'),
  );
  $form['sitemap_alias_other_settings']['sitemap_alias_group_titles']['sitemap_alias_group_title_taxonomies'] = array(
    '#type' => 'textfield',
    '#title' => t('Taxonomy group title'),
    '#default_value' => variable_get('sitemap_alias_group_title_taxonomies', t('Terms')),
    '#description' => t('The subtitle that will appear above the taxonomies when selecting a seperate tree per type'),
  );
  $form['sitemap_alias_other_settings']['sitemap_alias_group_titles']['sitemap_alias_group_title_users'] = array(
    '#type' => 'textfield',
    '#title' => t('User group title'),
    '#default_value' => variable_get('sitemap_alias_group_title_users', t('Users')),
    '#description' => t('The subtitle that will appear above the users when selecting a seperate tree per type'),
  );
    
  $form['sitemap_alias_other_settings']['sitemap_alias_list_type'] = array(
    '#type' => 'radios',
    '#title' => t('List type'),
    '#description' => t('Applies to the elements in the sitemap tree, whether you select one tree or a seperate tree per type. Ordered list will be numbered.'),
    '#options' => array('ul' => 'Unordered list', 'ol' => 'Ordered list'),
    '#default_value' => variable_get('sitemap_alias_list_type', 'ul'),
  );
  
  $form['sitemap_alias_other_settings']['sitemap_alias_sort'] = array(
    '#type' => 'radios',
    '#title' => t('Sort aliases alphabetically.'),
    '#description' => t('Selecting \'Do not sort\' will build the sitemap tree in the order the aliases were added to the site. 
    	<br/> Selecting a sorting method will sort the url aliases (and not the titles of your pages!).'),
    '#options' => array('none' => t('Do not sort'), 'asc' => t('Ascending'), 'desc' => t('Descending')),
    '#default_value' => variable_get('sitemap_alias_sort', 'none'),
  );
   
  $form['sitemap_alias_other_settings']['sitemap_alias_show_duplicates'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show duplicate aliases'),
    '#prefix' => '<label for="edit-sitemap-alias-show-duplicates">Show duplicates</label>',
    '#description' => t('Checking this box will show all existing aliases in the sitemap. Unchecking this box, will show only the last alias for each if multiple exist.'),
    '#default_value' => variable_get('sitemap_alias_show_duplicates', 0),
  );

  return system_settings_form($form, FALSE);
}
